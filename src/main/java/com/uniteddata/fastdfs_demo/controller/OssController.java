//package com.uniteddata.fastdfs_demo.controller;
//
//import cn.hutool.core.util.ZipUtil;
//import com.uniteddata.fastdfs_demo.entity.ResultBean;
//import com.uniteddata.showantzurecloud.common.bean.ResultBean;
//import com.uniteddata.showantzurecloud.showmanagement.constant.ContentTypeMapping;
//import com.uniteddata.showantzurecloud.showmanagement.constant.FileCodeConstant;
//import com.uniteddata.showantzurecloud.showmanagement.entity.FileCode;
//import com.uniteddata.showantzurecloud.showmanagement.util.GeneratorUtil;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//import java.nio.channels.FileChannel;
//import java.sql.Timestamp;
//import java.time.LocalDateTime;
//
///**
// * @ClassName OssController
// * @Description TODO
// * @Author dong <feng.db@uniteddata.com>
// * @Version 1.0.0
// **/
//@RestController
//@CrossOrigin(origins = "*")
//@RequestMapping("/oss")
//public class OssController {
//
//    private final String TEMP_FOLDER = System.getProperty("user.dir") + "/temp";
//    private final String URI_FOLDER = System.getProperty("user.dir") + "/uri";
//
//    @PostMapping("/svf")
//    public ResultBean uploadZsvf(MultipartFile zsvf, Long userId) throws IOException {
//        File folder = new File(TEMP_FOLDER);
//        if (!folder.exists()){
//            folder.mkdirs();
//        }
//        String originalFilename = zsvf.getOriginalFilename();
//        if (!".svfzip".equals(originalFilename.substring(originalFilename.lastIndexOf("."),originalFilename.length()))){
//            throw new RuntimeException("不支持该模型的压缩包，压缩包为.svfzip");
//        }
//        String extension = ".zip";
//        //保存在temp路径
//        String fileName = GeneratorUtil.generateShort8Uuid();
//        String fileFullName = fileName  + extension;
//        zsvf.transferTo(new File(TEMP_FOLDER + "/" + fileFullName));
//        //创建解压缩文件夹
//        folder = new File(URI_FOLDER + "/" + userId + "/" + FileCodeConstant.SVF + "/" + fileName);
//        if (!folder.exists()){
//            folder.mkdirs();
//        }
//        //解压缩到uri的userId的svf文件夹中
//        ZipUtil.unzip(TEMP_FOLDER + "/" + fileFullName,URI_FOLDER + "/" + userId + "/" + FileCodeConstant.SVF + "/" + fileName + "/");
//        //删除temp下临时文件
//        new File(TEMP_FOLDER + "/" + fileFullName).delete();
//        //返回保存的访问路径
//        FileCode fileCode = FileCode.builder()
//                .resourceUri("/" + fileName)
//                .time(Timestamp.valueOf(LocalDateTime.now()).getTime())
//                .build();
//        return ResultBean.succeed(fileCode);
//    }
//
//    @PostMapping("/upload/{type}")
//    public ResultBean upload(MultipartFile file, Long userId, @PathVariable String type) throws IOException {
//        //保存文件到userid的type下
//        File folder = new File(URI_FOLDER + "/" + userId + "/" + type);
//        if (!folder.exists()){
//            folder.mkdirs();
//        }
//        String originalFilename = file.getOriginalFilename();
//        String fileName = GeneratorUtil.generateShort8Uuid();
//        String extension = originalFilename.substring(originalFilename.lastIndexOf("."),originalFilename.length());
//        String fileFullName = fileName + extension;
//        file.transferTo(new File(URI_FOLDER + "/" + userId + "/" + type + "/" + fileFullName));
//        FileCode fileCode = FileCode.builder()
//                .resourceUri("/" + fileFullName)
//                .time(Timestamp.valueOf(LocalDateTime.now()).getTime())
//                .build();
//        return ResultBean.succeed(fileCode);
//    }
//
//    @GetMapping(value = {"/download/{type}/{uid}/{path}/{fileName:.+}"})
//    public void download(@PathVariable String fileName, @PathVariable Long uid, @PathVariable String type, HttpServletRequest request, HttpServletResponse response, @PathVariable(required = false) String path){
//        System.out.println(path+","+fileName);
//        String fileFullPath = "";
//        if(path == null || "temp".equals(path)){
//            fileFullPath = URI_FOLDER + "/" + uid + "/" + type + "/" + fileName;
//        }else{
//            fileFullPath = URI_FOLDER + "/" + uid + "/" + type +"/"+ path + "/" + fileName;
//        }
//        System.out.println(fileFullPath);
//        File file = new File(fileFullPath);
//        if (!file.exists()){
//            throw new RuntimeException("文件资源不存在");
//        }
//        String extension = fileName.substring(fileName.lastIndexOf("."), fileName.length());
//        String contentType = ContentTypeMapping.responseContentTypeMap().get(extension);
//        System.out.println(contentType);
//        if (".mp4".equals(extension)){
//            response.addHeader("Accept-Ranges","bytes");
//            response.setHeader("Content-Disposition", "attachment;fileName=" + file.getName());
//        }
//        if (contentType == null) {
//            response.setContentType("application/octet-stream");
//            response.addHeader("Accept-Ranges","bytes");
//        }else {
//            response.setContentType(contentType);
//        }
//        response.setContentLengthLong(file.length());
//        long pos = 0;
//        if (null != request.getHeader("Range")) {
//            // 断点续传
//            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
//            try {
//                pos = Long.parseLong(request.getHeader("Range").replaceAll("bytes=", "").replaceAll("-", ""));
//            } catch (NumberFormatException e) {
//                pos = 0;
//            }
//        }
//        response.setHeader("Content-Range", new StringBuffer("bytes ").append(pos + "").append("-")
//                .append((file.length() - 1) + "").append("/").append(file.length() + "").toString());
//        //response.addHeader("Content-Disposition", "attachment;filename=" + file.getName());
//        int bufferSize = 2097152;
//        FileInputStream fileInputStream = null;
//        FileChannel fileChannel = null;
//        ByteBuffer buff = null;
//        try {
//            fileInputStream = new FileInputStream(file);
//            fileInputStream.skip(pos);
//            fileChannel = fileInputStream.getChannel();
//            buff = ByteBuffer.allocateDirect(4194304);
//            byte[] byteArr = new byte[bufferSize];
//            int nRead, nGet;
//            while ((nRead = fileChannel.read(buff)) != -1) {
//                if (nRead == 0) {
//                    continue;
//                }
//                buff.position(0);
//                buff.limit(nRead);
//                while (buff.hasRemaining()) {
//                    nGet = Math.min(buff.remaining(), bufferSize);
//                    buff.get(byteArr, 0, nGet);
//                    response.getOutputStream().write(byteArr);
//                }
//                buff.clear();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new RuntimeException("文件下载失败！");
//        } finally {
//            buff.clear();
//            if (fileChannel!=null) {
//                try {
//                    fileChannel.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (fileInputStream != null) {
//                try {
//                    fileInputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//}
