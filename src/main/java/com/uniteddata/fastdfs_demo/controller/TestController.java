package com.uniteddata.fastdfs_demo.controller;


import com.uniteddata.fastdfs_demo.entity.ResultBean;
import com.uniteddata.fastdfs_demo.fastdfs.FastDfsUtils;
import jdk.nashorn.internal.objects.annotations.Property;
import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.omg.CORBA.ParameterModeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/test")
@RestController
public class TestController {

    @Value("${file.upload.bestSize}")
    private long bestSize;

    @Autowired
    private FastDfsUtils fastDfsUtils;

    @PostMapping("/upload")
    public ResultBean upload(MultipartFile file){

        String filename = file.getOriginalFilename();
        String substring = filename.substring(filename.lastIndexOf(".")+1);
        try {
            String url = fastDfsUtils.fileUpLoad(file.getBytes(), substring);
            if (!(url.equals("")&& url==null)){
                Map<String,String> data = new HashMap<>();
                data.put("url",url);
                return ResultBean.succeed(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ResultBean.defeated();
        } catch (MyException e) {
            e.printStackTrace();
            return ResultBean.defeated();
        }
        return ResultBean.succeed();
    }

    @RequestMapping("/testUpload")
    public ResultBean testUpload(MultipartFile file){
        try {
            // 1、加载配置文件，配置文件中的内容就是tracker服务的地址（绝对路径）
            ClientGlobal.init("C:\\java\\fastdfs_demo\\src\\main\\resources\\fastdfs.conf");
            // 2、创建一个TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            // 3、使用阿哥TrackerClient对象创建连接，获得一个TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            // 4、创建一个StorageServer的引用，值为null
            StorageServer storageServer = null;
            // 5、创建一个StorageClient对象，需要两个参数TrackerServer对象、Storage、Server的引用
            StorageClient storageClient = new StorageClient(trackerServer, storageServer);
            // 6、使用StorageClient对象上传图片,扩展名不用带“.”
            String [] strs = storageClient.upload_file("C:\\img\\image2.jpg","jpg",null);
            // 7、返回数组，包含组名和图片的路径
            System.out.print("地址为：");
            String path = "";
            for (String str : strs) {   // 组名+磁盘地址
                path = path + str + "/";
            }
            // 进行地址处理并输出
            System.out.println(path.substring(0,path.length()-1));
            return ResultBean.succeed(path);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultBean.defeated();
        }
    }

    @GetMapping("/download")
    public void downloadFile(String fileUrl, HttpServletResponse response){
        fastDfsUtils.downloadFile(fileUrl,response);
    }

}
