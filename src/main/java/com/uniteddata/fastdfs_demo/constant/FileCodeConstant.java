package com.uniteddata.fastdfs_demo.constant;

/**
 * @ClassName FileCodeConstant
 * @Description TODO
 * @Author dong <feng.db@uniteddata.com>
 * @Version 1.0.0
 **/
public class FileCodeConstant {
    public final static String SVF = "svf";
    public final static String PDF = "pdf";
    public final static String SEVEN_TWO_ZERO = "720";
    public final static String video = "video";
    public final static String THREE_DIMENSIONAL_SCANNING = "3d";
}
