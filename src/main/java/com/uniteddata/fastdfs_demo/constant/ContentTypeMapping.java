package com.uniteddata.fastdfs_demo.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ContentTypeMapping
 * @Description TODO
 * @Author dong <feng.db@uniteddata.com>
 * @Version 1.0.0
 **/
public class ContentTypeMapping {
    public static Map<String,String> responseContentTypeMap(){
        return new HashMap<String,String>(){
            {
                this.put(".pdf","application/pdf");
                this.put(".png","image/png");
                this.put(".jpg","image/jpeg");
                this.put(".jpeg","image/jpeg");
                this.put(".gif","image/gif");
                this.put(".html", "text/html");
                this.put(".mp4","video/mpeg4");
            }
        };
    }
}
