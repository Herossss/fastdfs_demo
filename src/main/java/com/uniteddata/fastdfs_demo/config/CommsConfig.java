package com.uniteddata.fastdfs_demo.config;


        import com.uniteddata.fastdfs_demo.fastdfs.FastDfsUtils;
        import org.csource.common.MyException;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;

        import java.io.IOException;

@Configuration
public class CommsConfig {

    @Bean
    public FastDfsUtils getFastDfsUtils() throws IOException, MyException {
        FastDfsUtils fastDfsUtils = new FastDfsUtils();
        return fastDfsUtils;
    }
}

