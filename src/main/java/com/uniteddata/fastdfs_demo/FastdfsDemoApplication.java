package com.uniteddata.fastdfs_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication
public class FastdfsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastdfsDemoApplication.class, args);
    }

}
