package com.uniteddata.fastdfs_demo.fastdfs;


import com.uniteddata.fastdfs_demo.constant.ContentTypeMapping;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class FastDfsUtils {
    private TrackerClient trackerClient;
    private TrackerServer trackerServer;
    private StorageServer storageServer = null;
    private StorageClient storageClient = null;


    public FastDfsUtils() throws IOException, MyException {

        String filePath = new ClassPathResource("fastdfs.conf").getFile().getAbsolutePath();
        ClientGlobal.init(filePath);
        trackerClient = new TrackerClient();
        trackerServer = trackerClient.getConnection();
        storageClient = new StorageClient(trackerServer,storageServer);
    }

    public String fileUpLoad(byte[] bytes,String file_end) throws IOException, MyException {
        return  fileUpLoad(bytes,file_end,null);
    }

    public String fileUpLoad(byte[] bytes, String file_end, NameValuePair[] nameValuePairs) throws IOException, MyException {
        String[] strings = storageClient.upload_file(bytes, file_end, nameValuePairs);
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            stringBuffer.append(strings[i]);
            if(i==0){
                stringBuffer.append("/");
            }
        }
        return stringBuffer.toString();
    }

    public void downloadFile(String fileUrl, HttpServletResponse response) {
        String key = fileUrl.substring(fileUrl.lastIndexOf("."));
        String contenType = ContentTypeMapping.responseContentTypeMap().get(key);
        response.setContentType(contenType);
        response.addHeader("Content-Disposition", "attachment;filename*=utf-8'zh_cn'" + fileUrl);
        try {
            String[] strings = fileUrl.split("\\/",2);
            byte[] b = storageClient.download_file(strings[0], strings[1]);
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

